
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.
Experiment compares the performance in terms of throughput of two tcp variant cubic and reno in a 4 router topolgy when delay and attack are introduced in the network.


2) Does the project generally follow the guidelines and parameters we have
learned in class?
Yes the project follows general guidelines and parameter we have discussed.




## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?
The goal is to compare the performance of TCP variants reno and cubic when delay or attack is introducted in a 4 router topology. The goal useful as it
it introduces delay and attack in the network but it could have been better if some more parameters were introduced in the experiment like buffer size,drop rate etc as well as more tcp variant.


2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?
The metrices chosen complement the experimental goal but the design of experiment could have been something different and more interesting. A 4 node router
topology is very comman.


3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?
The experiment is very simple and doesnot require more than 2 trails. They have taken 3 trials, I think that will give more accuracy.



4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?
I think the author should have considered responce time as one of the metrices


5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?
The parameters are meaningful.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?
There is no interaction suggested in the experiment.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?
The comparisons are reasonably.


## Communicating results


1) Do the authors report the quantitative results of their experiment?
Yes the author has performed 3 trials changing different parameters for the experiment and taken an average of those 3 trials for accuracy.


2) Is there information given about the variation and/or distribution of
experimental results?
Yes there is information given about variation but they could have presented it in a better way by using R studio.


3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?
They have put throughput values for each experiment unit and directly compared throughput, there are no ratio games or other artificial practices


4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?
Data is presented in quite a simple and understandable way, very unambiguous but they have avoided graphs or any plots which could have been better


5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?
In my opionion Cubic has better performance than reno overall and reno works closely as cubic when delay is introduced.


## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?
The instructions are very clear and easy to understand.


2) Were you able to successfully produce experiment results?
Yes I was able to successfully produce the experiment results

3) How long did it take you to run this experiment, from start to finish?
It took me about an hour


4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
No additional steps

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
5


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

Using R studio for data analysis and intoducing more parameters would have been more intersting.
