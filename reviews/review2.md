
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.

 This experiment was designed to compare two tcp variants (cubic and reno) under four different situations.
 Basically the experimenters are measuring the throughput of communication from client to server in OSPF 4 node topology.
 They have four different cases.
 Case 1: Case with no delay and link faliure
 Case 2: Case with link faliure but no delay
 Case 3: Case with delay but no faliure
 Case 4: Case with both delay and a faliure

2) Does the project generally follow the guidelines and parameters we have
learned in class?

 Although, the experimenters have taken care of being conscise and to the point and generally a clear representation of what they are doing by following some of the rules mentioned in the class but
 the design of the experiment is not very reproducable and does not capture the essence of the goal (will mention about it in detail below).
 Also, there is no visualization used in the experiement. A box plot, violin or throughput vs time would have been a good representative of their results.



## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

 The goal is focused and it tells us about the outline of the project and the different situations in which a
 project will be implemented but it does not specifically tells us that what metric are they going to measure.
 Performance is a very broad term. Also, it doesn't clearly mention the parameters they want to change.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

 Throughput is a good metric that they are measuring. In terms of the parameters that they are changing delay is a ok but the link failiure is not very reproducable.
 The reason being that once we are running a trial for say cubic_nd_a_1.txt when we bring the eth1 (The report has said eth3 for router2 which is wrong, it works with bringing
 down eth1) down, OSPF recomputes and alternative path. Now, if we do the second trial we cannot repeat it (even if we bring eth1 for router 2 back up) because it has recomputed
 a new path. And if we bring router 4 interface down, it takes a long time for it to recompute an alternative path back to router1-router2-router3. Hence, if they wanted to go that route
 they should have provided more documentation as to how to go about this problem.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

 Case 1 and Case 3 are reasonable. But case 2 and Case 4 have the link faliure problem as mentioned above. One more issue with these cases is that
 since I could fail the link in anytime during the 10 sec period, one trial might fail a link at around 2 sec the other trial maybe around 9. Hence, it is
 not very reporducable.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

 The metric itself (throughput) is a good metric to study and is a good representative of the performance of a TCP variant. Other metrics could have
 been the reseillience time.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?

 The delay is a good parameter to choose, which mimcs a long RTT. But a link failure and recovery from it is effected more by routing protocol (OSPF and RIP) than the TCP variant.
 It is not clear that by measuring an avg throuput from failing a link what are the project members trying to achieve. If they are trying to see that how fast the throughput stabalizes back to
 it's original value upon interruption than they shouldn't have used an avg throughput. Instead they should have plotted the throughput vs time.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

 It seems that the possibility of interractions b/w parameters is not that much, so they haven't mentioned about it.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

 While case 1 and Case 3 gives us some reasonable comparison. Case 2 and Case 4 is not a realistic comparison because it has a lot of variability to it and is also not a fair
 criteria to compare these two tcp variants (Reasons have been mentioned above).

## Communicating results


1) Do the authors report the quantitative results of their experiment?

 Yes, the authors do report the quantitative results of their experiment.

2) Is there information given about the variation and/or distribution of
experimental results?

 The project members have stored the iperf data files, they show a raw distrubtion of the througput but they haven't communicated that results. They have communicated the avg throughput.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?
 Yes, they have practiced data integrity. The average throuput in all the cases were presented accurately.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?

 The data is presented in an understandable way. But, it would have been better if the average throughput was supported by a violin or box plot. It would should the major distribution of the data.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

 Yes the conclusion drawn by the authors about more throughput of cubic then reno is correct based on the experimental results. But, as mentioned earlier Avg througput is not the greatest way to communicate the results
 of this experiment.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

Raw data -> Results: The data is given in the data folder. They haven't mentioned that how to get the avergae throughput from that data

Existing experiment setup -> Data: They have mentioned how to get the data from the existing set up. A little bit more detailed steps would have been better rather than stating " as done in previous labs"

Set up experiment: They have mentioned clearly how to setup the experiment and resources for the experiment.

2) Were you able to successfully produce experiment results?

I was able to successfully reproduce the experiment. Case 1 and Case 3 were easier but Case 2 and Case 4 are hard to reprodce as mentioned in the pervious question.

3) How long did it take you to run this experiment, from start to finish?

 It took around 4 hours to reproduce the whole experiment.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

 Yes, I had to do additional stpes to complete this experiment. In Case 2 and Case 4 I had to figure out what is the correct ethernet interface to deactivate because eth3 didn't have any effect on iperf. Also, Once I had put eth 1 down for router 2, it had chosen an alternate path
 because to bring it back to the old path I had to put down the router 4 interface as well so that it brings it back to the original path for the next trial. This wasn't documented properly hence caused me extra time to figure out how to do it. It took about an hour of work more to
 do this for every trial.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

 This would have a degree 3 reproducability because it required considrable effort from my side.


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

 Generally the authors made a good effort in their experiment. But I would advise the authors of this project

 * to document in detail and don't assume that the reviewer would know everything.
 * to add visualization to support their results. (Average througput doesn't tell a lot in this situation).
 * to do a good research before starting an experiment and understand what they are trying to achieve. (failing a link and measuring average througput doesn't contribute a lot in this study)
