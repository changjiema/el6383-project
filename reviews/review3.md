
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.
Ans: The project is about comparing TCP reno with TCP cubic by varying parameters which are delay and attack. The two TCP variants are going to be compared by measuring throughput. In the delay part the delay of the link is to be changed and the attack which includes the node going up and down. There are four cases each of which the parameters are changed and three readings for each of the TCP variants is taken.




2) Does the project generally follow the guidelines and parameters we have learned in class?
Ans: Yes the project generally follows the guidelines and parameters learnt in class, except for the goal not being that clear. According to me the goal is clear in what is to be done but the word used for node going up and down (attack) is little strange. Other than this other guidelines are properly followed, that is the parameters to be varied are clearly specified and the metrics that is to be measured is also clearly mentioned.  




## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal? Is it useful and likely to have interesting results?
Ans: The goal of the experiment is to “compare TCP reno and cubic by changing delay and attack(node being up or down). Yes this is a focused and specific goal, because what is to be done is clearly mentioned that is comparing the TCP variants. Also what parameters are to be changed are also mentioned.
Yes it is useful because by this experiment we would get to know which of the TCP variants is better and which one should be used in different environments. The results nevertheless would not be interesting because by theory we already know which of the TCP variant is better.

2) Are the metric(s) and parameters chosen appropriate for this experiment goal? Does the experiment design clearly support the experiment goal?
Ans: Yes the metric and parameters are appropriate for this experiment because throughput is a correct measure to gauge which TCP variant is better. Since throughput is data/time it is the most apt measurement to take for comparing the two.
Also the parameter: delay chosen are also apt, since delay is an integral part to any environment. Second parameter chosen which attack according to me is not that relevant to the experiment. There are many more parameters that affect the throughput more than the link being up or down. Bandwidth available could be one alternative.


3) Is the experiment well designed to obtain maximum information with the minimum number of trials?
Ans: Yes the experiment is well designed to obtain the information with minimum number of trials. Here only 3 trials are required to gauge about the performance of a single TCP variant in each case. Here Iperf is used for the measurements. Iperf gives clearly what the throughput is any condition which requires minimum number of trials.


4) Are the metrics selected for study the *right* metrics? Are they clear, unambiguous, and likely to lead to correct conclusions? Are there other metrics that might have been better suited for this experiment?
Ans: The metric used is correct for the study of this experiment. They are clear and do lead to the right conclusions. Yes there are other metrics that might have been better suited for this experiment such as congestion window. Nevertheless the metric that is selected in this experiment is also apt and unambiguous.

5) Are the parameters of the experiment meaningful? Are the ranges over which parameters vary meaningful and representative?
Ans: Yes the parameters of the experiment are meaningful. The parameters chosen here are attack and delay. Delay is always a factor in any link when TCP data is being sent and it always plays a role in sending of data. The range is to be selected by us in the experiment.
The second parameter attack is also meaningful, because depending on the links active the throughput changes. Depending on the number of links active or inactive the throughput changes. There are just two ranges of this parameter, either up or down.


6) Have the authors sufficiently addressed the possibility of interactions between parameters?
Ans: The authors have addressed the possibility of interactions between parameters. They have selected all the cases in which the delay is changed or the attack is changed or both the parameters are changed. In this way they have shown all the possible interactions.



7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate and realistic?
Ans: Yes the comparisons are made reasonable. The comparisons are between two TCP variants are made in the same network environments by changing the same parameters in the same conditions. The measurements are also made in the same conditions.
Yes the baseline selected is appropriate and realistic because delay and the link status are also used in the real environment.



## Communicating results


1) Do the authors report the quantitative results of their experiment?
Ans: Yes the authors have represented the quantitative result as a table at the end of the experiment. They have represented it in a table format which is easy to read and analyze. Also they have given the average of all the three readings of various cases.

2) Is there information given about the variation and/or distribution of experimental results?
Ans: No there is no information given about the variation or distribution of experimental results.


3) Do the authors practice *data integrity* - telling the truth about their data, avoiding ratio games and other practices to artificially make their results seem better?
Ans: Yes the authors do practice date integrity. They are telling the truth about their data since they are straight away reporting the text files which they are storing through the iperf data. No other practices are used to make one TCP variant better than the other.

4) Is the data presented in a clear and effective way? If the data is presented in graphical form, is the type of graph selected appropriate for the "story" that the data is telling?
Ans: Yes the data is presented in a clear way with all the names being very clear and descriptive of the file that is saved. This makes it easy for us to look at the files and know which case the file is for. No the data is not represented in a graphical form.


5) Are the conclusions drawn by the authors sufficiently supported by the experiment results?
Ans. Yes the conclusion drawn is totally supported by the experimental results. The results are presented in a tabular form as mentioned above and it is pretty easy to draw the conclusion that the authors have drawn from it.
The conclusion is straight away inferred from the table of results that is presented by the authors at the end of the experiment.


## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results, Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear and easy to understand?
Ans: Yes the author has included clear instructions on how to setup the experiment. The instructions are very clear and well outlined, and commands are given clearly for each and every step that is need to be performed. It is pretty simple to perform the experiment from the given data.

2) Were you able to successfully produce experiment results?
Ans. Yes I was successfully able to produce experiment results.


3) How long did it take you to run this experiment, from start to finish?
Ans. It took me about 5 and a half hours to finish the experiment from start to finish.


4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
Ans: No I did not need to make any changes or perform any additional steps beyond what was given to complete this experiment. I could complete the experiment with what was given in the document.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
Ans: I would characterize this experiment at a degree of five of reproducibility.




## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.
